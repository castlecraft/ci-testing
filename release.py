import json
import sys

def update_version(json_file_path, new_version):
    try:
        # Read the JSON file
        with open(json_file_path, 'r') as file:
            data = json.load(file)

        # Update the version
        data['version'] = new_version

        # Write the updated data back to the JSON file
        with open(json_file_path, 'w') as file:
            json.dump(data, file, indent=4)

        print(f"Version updated to {new_version} in {json_file_path}")
    except Exception as e:
        print(f"Error updating version: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <new_version>")
        raise BaseException("Found invalid parameters only 1 allowed.")

    json_file_path = "version.json"
    new_version = sys.argv[1]

    update_version(json_file_path, new_version)
